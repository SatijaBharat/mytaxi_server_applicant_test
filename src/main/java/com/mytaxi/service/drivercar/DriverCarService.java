package com.mytaxi.service.drivercar;

import com.mytaxi.domainobject.DriverCarDO;

public interface DriverCarService
{

    void delete(DriverCarDO driverCar);


    DriverCarDO save(DriverCarDO driverCar);


    DriverCarDO findByDriverIdAndCarId(final Long driverId, final Long carId);

}
