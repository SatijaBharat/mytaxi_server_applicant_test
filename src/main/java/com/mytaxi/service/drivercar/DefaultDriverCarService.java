package com.mytaxi.service.drivercar;

import com.mytaxi.dataaccessobject.DriverCarRepository;
import com.mytaxi.domainobject.DriverCarDO;
import org.springframework.stereotype.Service;

@Service
public class DefaultDriverCarService implements DriverCarService
{


    private final DriverCarRepository driverCarRepository;

    public DefaultDriverCarService(DriverCarRepository driverCarRepository){
        this.driverCarRepository = driverCarRepository;
    }


    @Override
    public void delete(DriverCarDO driverCar)
    {
        driverCarRepository.delete(driverCar);
    }


    @Override
    public DriverCarDO save(DriverCarDO driverCar)
    {
        return driverCarRepository.save(driverCar);
    }


    @Override
    public DriverCarDO findByDriverIdAndCarId(Long driverId, Long carId)
    {
        return driverCarRepository.findByDriverIdAndCarId(driverId, carId);
    }

}
