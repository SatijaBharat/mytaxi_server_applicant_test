package com.mytaxi.service.driver;

import com.mytaxi.dataaccessobject.DriverRepositoryCustom;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.OnlineStatus;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@Repository
public class DriverRepositoryCustomImpl implements DriverRepositoryCustom
{
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<DriverDO> find(OnlineStatus onlineStatus, String username, String licensePlate, Short seatCount, Float rating, String engineType) {

        StringBuilder query = new StringBuilder("SELECT D from DriverDO D " +
            "LEFT JOIN DriverCarDO DC on DC.driverId = D.id " +
            "LEFT JOIN CarDO C on C.id = DC.carId " +
            "WHERE 1 = 1 ");

        if(onlineStatus != null) {
            query.append("AND D.onlineStatus = :onlineStatus ");
        }
        if(!StringUtils.isEmpty(username)) {
            query.append("AND D.username = :username ");
        }
        if(!StringUtils.isEmpty(licensePlate)) {
            query.append("AND C.licensePlate = :licensePlate ");
        }
        if(seatCount != null) {
            query.append("AND C.seatCount = :seatCount ");
        }
        if(rating != null) {
            query.append("AND C.rating >= :rating ");
        }
        if(engineType != null) {
            query.append("AND C.engineType = :engineType ");
        }

        Query query1 = em.createQuery(query.toString());
        if(onlineStatus != null) {
            query1.setParameter("onlineStatus", onlineStatus);
        }
        if(!StringUtils.isEmpty(username)) {
            query1.setParameter("username", username);
        }
        if(!StringUtils.isEmpty(licensePlate)) {
            query1.setParameter("licensePlate", licensePlate);
        }
        if(seatCount != null) {
            query1.setParameter("seatCount", seatCount);
        }
        if(rating != null) {
            query1.setParameter("rating", rating);
        }
        if(engineType != null) {
            query1.setParameter("engineType", engineType);
        }
        return query1.getResultList();
    }
}
