package com.mytaxi.service.driver;

import com.mytaxi.controller.mapper.CarMapper;
import com.mytaxi.controller.mapper.DriverMapper;
import com.mytaxi.dataaccessobject.DriverCarRepository;
import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.dataaccessobject.DriverRepositoryCustom;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.datatransferobject.DriverSearchCriteriaDTO;
import com.mytaxi.domainobject.DriverCarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.GeoCoordinate;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.DriverNotOnlineException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.drivercar.DriverCarService;
import java.util.ArrayList;
import java.util.List;

import com.mytaxi.service.car.CarService;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
public class DefaultDriverService implements DriverService
{

    private static final Logger LOG = LoggerFactory.getLogger(DefaultDriverService.class);

    private final Object LOCK = new Object();

    private final DriverRepository driverRepository;

    private final DriverCarService driverCarService;

    private final DriverRepositoryCustom driverRepositoryCustom;

    private final CarService carService;


    public DefaultDriverService(final DriverRepository driverRepository,CarService carService,
        final DriverCarService driverCarService, final DriverRepositoryCustom driverRepositoryCustom)
    {
        this.driverRepository = driverRepository;
        this.carService = carService;
        this.driverCarService = driverCarService;
        this.driverRepositoryCustom = driverRepositoryCustom;
    }


    /**
     * Selects a driver by id.
     *
     * @param driverId
     * @return found driver
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    public DriverDO find(Long driverId) throws EntityNotFoundException
    {
        return findDriverChecked(driverId);
    }


    /**
     * Creates a new driver.
     *
     * @param driverDO
     * @return
     * @throws ConstraintsViolationException if a driver already exists with the given username, ... .
     */
    @Override
    public DriverDO create(DriverDO driverDO) throws ConstraintsViolationException
    {
        DriverDO driver;
        try
        {
            driver = driverRepository.save(driverDO);
        }
        catch (DataIntegrityViolationException e)
        {
            LOG.warn("ConstraintsViolationException while creating a driver: {}", driverDO, e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return driver;
    }


    /**
     * Deletes an existing driver by id.
     *
     * @param driverId
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    @Transactional
    public void delete(Long driverId) throws EntityNotFoundException
    {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setDeleted(true);
    }


    /**
     * Update the location for a driver.
     *
     * @param driverId
     * @param longitude
     * @param latitude
     * @throws EntityNotFoundException
     */
    @Override
    @Transactional
    public void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException
    {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setCoordinate(new GeoCoordinate(latitude, longitude));
    }


    /**
     * Find all drivers by online state.
     *
     * @param onlineStatus
     */
    @Override
    public List<DriverDO> findDriversByFilterCriteria(OnlineStatus onlineStatus, String username, String licensePlate, Short seatCount, Float rating, String engineType)
    {
        return driverRepositoryCustom.find(onlineStatus, username, licensePlate, seatCount, rating, engineType);
    }


    //    @Override
    //    public List<DriverDTO> findDriversByFilterCriteria(DriverSearchCriteriaDTO driverSearchDTO) throws EntityNotFoundException
    //    {
    //        List<DriverDO> drivers;
    //        try
    //        {
    //
    //            drivers = driverCarRepository.findDriverByFilterCriteria(driverSearchDTO);
    //        }
    //        catch (Exception e)
    //        {
    //            throw new EntityNotFoundException("Driver entity not found ");
    //        }
    //
    //        return DriverMapper.makeDriverDTOList(drivers);
    //    }

    private DriverDO findDriverChecked(Long driverId) throws EntityNotFoundException
    {
        return driverRepository.findById(driverId)
            .orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + driverId));
    }


    /**
     * Select a car for the driver.
     * @param driverId
     * @param carId
     *
     * @throws EntityNotFoundException
     * @throws DriverNotOnlineException
     * @throws CarAlreadyInUseException
     */
    @Transactional
    public void selectCar(long driverId, long carId)
        throws EntityNotFoundException, DriverNotOnlineException, CarAlreadyInUseException
    {
        DriverDO driver = findDriverChecked(driverId);
        carService.find(carId);

        if ( isNotOnline(driver) )
        {
            throw new DriverNotOnlineException("Status of driver with Id ["+ driverId +" is not ONLINE.");
        }

        synchronized (LOCK){

            if( driverCarService.findByDriverIdAndCarId(driverId, carId) != null )
            {
                throw new CarAlreadyInUseException("Car with Id [" + carId + "] is already assigned to a driver.");
            }

            DriverCarDO driverCarDO = new DriverCarDO(driverId, carId);
            driverCarService.save(driverCarDO);
        }

    }



    /**
     * De-select a car.
     * @param driverId
     *
     * @throws EntityNotFoundException
     */
    @Transactional
    public void deselectCar(long driverId, long carId) throws EntityNotFoundException
    {

        DriverCarDO driverCar = driverCarService.findByDriverIdAndCarId(driverId, carId);

        if (driverCar == null)
        {
            throw new EntityNotFoundException("Driver with Id ["+ driverId +"] and car assigned with Id ["+ carId +"] not found.");
        }

        driverCarService.delete(driverCar);

    }


    private boolean isNotOnline(DriverDO driver)
    {
        return !(OnlineStatus.ONLINE.equals(driver.getOnlineStatus()));
    }

}
