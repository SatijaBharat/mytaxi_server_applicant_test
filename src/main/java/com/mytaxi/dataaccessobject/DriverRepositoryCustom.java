package com.mytaxi.dataaccessobject;

import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.OnlineStatus;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 * Database Access Object for driver table.
 * <p/>
 */
public interface DriverRepositoryCustom
{


    List<DriverDO> find(OnlineStatus onlineStatus, String username, String licensePlate, Short seatCount, Float rating, String engineType);

}
