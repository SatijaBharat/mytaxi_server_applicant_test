package com.mytaxi.dataaccessobject;

import com.mytaxi.datatransferobject.DriverSearchCriteriaDTO;
import com.mytaxi.domainobject.DriverCarDO;
import com.mytaxi.domainobject.DriverDO;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface DriverCarRepository extends CrudRepository<DriverCarDO, Long>
{

    DriverCarDO findByDriverIdAndCarId(final Long driverId, final Long carId);


}
