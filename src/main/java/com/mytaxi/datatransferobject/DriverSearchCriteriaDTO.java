package com.mytaxi.datatransferobject;

import com.mytaxi.domainvalue.OnlineStatus;

public class DriverSearchCriteriaDTO 
{
	private String username;
	
	private OnlineStatus onlineStatus;
	
	private String licensePlate;
	
	private Float rating;


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public OnlineStatus getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(OnlineStatus onlineStatus) {
		this.onlineStatus = onlineStatus;
	}
	
	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "DriverSearchCriteriaDTO [username=" + username + ", onlineStatus=" + onlineStatus
				+ ", licensePlate=" + licensePlate + ", rating=" + rating + "]";
	}
	
}
