/**
 * CREATE Script for init of DB
 */

-- Create 3 OFFLINE drivers

insert into driver (id, date_created, deleted, online_status, password, username) values (1, now(), false, 'OFFLINE',
'driver01pw', 'driver01');

insert into driver (id, date_created, deleted, online_status, password, username) values (2, now(), false, 'OFFLINE',
'driver02pw', 'driver02');

insert into driver (id, date_created, deleted, online_status, password, username) values (3, now(), false, 'OFFLINE',
'driver03pw', 'driver03');


-- Create 3 ONLINE drivers

insert into driver (id, date_created, deleted, online_status, password, username) values (4, now(), false, 'ONLINE',
'driver04pw', 'driver04');

insert into driver (id, date_created, deleted, online_status, password, username) values (5, now(), false, 'ONLINE',
'driver05pw', 'driver05');

insert into driver (id, date_created, deleted, online_status, password, username) values (6, now(), false, 'ONLINE',
'driver06pw', 'driver06');

-- Create 1 OFFLINE driver with coordinate(longitude=9.5&latitude=55.954)

insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username)
values
 (7,
 'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'OFFLINE',
'driver07pw', 'driver07');

-- Create 1 ONLINE driver with coordinate(longitude=9.5&latitude=55.954)

insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username)
values
 (8,
 'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'ONLINE',
'driver08pw', 'driver08');


-- Create cars


insert into car (id, convertible, date_created, deleted, engine_type, license_plate, manufacturer, rating, seat_count)
VALUES (1, true, now(), false, 'ELECTRIC', 'KL 07 AS 4444', 'Mercedes', 5.0, 5);

insert into car (id, convertible, date_created, deleted, engine_type, license_plate, manufacturer, rating, seat_count)
VALUES (2, true, now(), false, 'ELECTRIC', 'KK 26 BC 1123', 'Mercedes', 4.0, 4);

insert into car (id, convertible, date_created, deleted, engine_type, license_plate, manufacturer, rating, seat_count)
VALUES (3, true, now(), false, 'GAS', 'TN 03 CE 4896', 'Mercedes', 3.0, 4);

insert into car (id, convertible, date_created, deleted, engine_type, license_plate, manufacturer, rating, seat_count)
VALUES (4, true, now(), false, 'GAS', 'MH 03 ZZ 8888', 'BMW', 3.0, 4);

insert into car (id, convertible, date_created, deleted, engine_type, license_plate, manufacturer, rating, seat_count)
VALUES (5, false, now(), false, 'ELECTRIC', 'DE 03 AZ 6666', 'BMW', 5.0, 5);

insert into car (id, convertible, date_created, deleted, engine_type, license_plate, manufacturer, rating, seat_count)
VALUES (6, false, now(), false, 'HYBRID', 'IN 03 AZ 1212', 'BMW', 2.0, 5);

insert into car (id, convertible, date_created, deleted, engine_type, license_plate, manufacturer, rating, seat_count)
VALUES (7, false, now(), false, 'ELECTRIC', 'HR 03 AZ 3659', 'AUDI', 4.0, 4);

insert into car (id, convertible, date_created, deleted, engine_type, license_plate, manufacturer, rating, seat_count)
VALUES (8, false, now(), false, 'GAS', 'EE 03 AZ 9745', 'AUDI', 2.0, 5);

insert into car (id, convertible, date_created, deleted, engine_type, license_plate, manufacturer, rating, seat_count)
VALUES (9, false, now(), false, 'HYBRID', 'GA 03 AZ 1212', 'AUDI', 3.0, 5);


insert into driver_car (id, driver_id,  car_id) VALUES (1, 4,	1);
insert into driver_car (id, driver_id,  car_id) VALUES (2, 5,	2);
insert into driver_car (id, driver_id,  car_id) VALUES (3, 6,	3);
insert into driver_car (id, driver_id,  car_id) VALUES (4, 8,	4);





