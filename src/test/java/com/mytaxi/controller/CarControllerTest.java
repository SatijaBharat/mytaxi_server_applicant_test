package com.mytaxi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytaxi.BootstrapDataForTest;
import com.mytaxi.MytaxiServerApplicantTestApplicationTests;
import com.mytaxi.controller.mapper.CarMapper;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.service.car.CarService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.hasSize;

@WebMvcTest
public class CarControllerTest extends BootstrapDataForTest
{

    private static final ObjectMapper mapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Mock
    private CarService carService;

    @InjectMocks
    private CarController carController;


    @BeforeClass
    public static void setup()
    {
        MockitoAnnotations.initMocks(CarController.class);
    }


    @Before
    public void init()
    {
        mockMvc = MockMvcBuilders.standaloneSetup(carController).build();
    }


    @Test
    public void testGetCar() throws Exception
    {
        CarDTO carData = getCarDTO();
        CarDO carDO = CarMapper.makeCarDO(carData);

        when(carService.find(1l)).thenReturn(carDO);

        mockMvc.perform(get("/v1/cars/{carId}", 1L))
            .andExpect(status().isOk())
            .andExpect(jsonPath("licensePlate").value("DE 03 AZ 6666"))
            .andExpect(jsonPath("engineType").value("ELECTRIC"));

    }


    @Test
    public void getAllCars() throws Exception
    {
        List<CarDO> cars = Collections.singletonList(getCar());

        when(carService.findAll()).thenReturn(cars);

        mockMvc.perform(get("/v1/cars"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[0].licensePlate").value("DE 03 AZ 6666"))
            .andExpect(jsonPath("$[0].engineType").value("ELECTRIC"));
    }
//
//
//    @Test
//    public void createCar() throws Exception
//    {
//        CarDTO carData = getCarDTO();
//        CarDO carDO = CarMapper.makeCarDO(carData);
//
//        String jsonInString = mapper.writeValueAsString(carData);
//        doReturn(carDO).when(carService).create(any(CarDO.class));
//        carController.createCar(carData);
//        MvcResult result =
//            mvc
//                .perform(
//                    post("/v1/cars")
//                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
//                        .content(jsonInString))
//                .andExpect(MockMvcResultMatchers.status().isCreated()).andReturn();
//
//        final String responseBody = result.getResponse().getContentAsString();
//        Assert.assertTrue(responseBody.contains("test"));
//    }
//
//
//
//    @Test
//    public void deleteCar() throws Exception
//    {
//        doNothing().when(carService).delete(any(Long.class));
//
//        carController.deleteCar(1L);
//
//        MvcResult result =
//            mvc
//                .perform(delete("/v1/cars/{carId}", 1L))
//                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
//
//        Assert.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
//    }



}
