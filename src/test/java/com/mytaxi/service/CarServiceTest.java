package com.mytaxi.service;

import com.mytaxi.BootstrapDataForTest;
import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.service.car.CarService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

public class CarServiceTest extends BootstrapDataForTest
{

    @Mock
    private CarRepository carRepository;


    @InjectMocks
    private CarService carService;


    @BeforeClass
    public static void setUp()
    {
        MockitoAnnotations.initMocks(CarService.class);
    }


    @Test
    public void testGetAllCars()
    {
        when(carService.findAll()).thenReturn(Collections.singletonList(getCar()));

        List<CarDO> cars = carService.findAll();

        verify(carRepository, times(1)).findAll();
    }




}
