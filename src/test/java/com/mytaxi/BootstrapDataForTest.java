package com.mytaxi;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverCarDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.GeoCoordinate;
import com.mytaxi.domainvalue.OnlineStatus;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.ZonedDateTime;

@RunWith(MockitoJUnitRunner.class)
public abstract class BootstrapDataForTest
{

    public CarDO getCar()
    {
        CarDO car = new CarDO();
        car.setId(1L);
        car.setLicensePlate("DE 03 AZ 6666");
        car.setSeatCount((short)4);
        car.setRating(5.0F);
        car.setDateCreated(ZonedDateTime.now());
        car.setEngineType(EngineType.ELECTRIC);
        car.setConvertible(true);
        return car;
    }



    public CarDTO getCarDTO()
    {
        return CarDTO
            .newBuilder()
            .setLicenseplate("DE 03 AZ 6666")
            .setRating(5.0F)
            .setEngineType(EngineType.ELECTRIC)
            .setSeatCount((short)4)
            .setConvertible(true)
            .createCarDTO();
    }


    public DriverDO getDriver()
    {
        DriverDO driver = new DriverDO();
        driver.setId(1L);
        driver.setDateCreated(ZonedDateTime.now());
        driver.setDeleted(false);
        driver.setUsername("test");
        driver.setPassword("test");
        driver.setOnlineStatus(OnlineStatus.ONLINE);
        GeoCoordinate geoCoordinate = new GeoCoordinate(50, 60);
        driver.setCoordinate(geoCoordinate);
        return driver;
    }


    public DriverDTO
    getDriverDTO()
    {
        GeoCoordinate geoCoordinate = new GeoCoordinate(50, 60);
        return DriverDTO
            .newBuilder()
            .setId(1L)
            .setPassword("test")
            .setUsername("test")
            .setCoordinate(geoCoordinate)
            .createDriverDTO();
    }


    public DriverCarDO getDriverCar()
    {
        DriverCarDO driverCar = new DriverCarDO();
        driverCar.setId(1L);
        driverCar.setDriverId(1L);
        driverCar.setCarId(1L);
        return driverCar;
    }
}
